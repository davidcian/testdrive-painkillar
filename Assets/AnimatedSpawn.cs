﻿using System;

using UnityEngine;

class AnimatedSpawn : MonoBehaviour
{
	public float startFactor = 0.1f, endFactor = 3f;
	public float sizeSpeed = 0.1f;

	public void Awake()
	{
		transform.localScale.Set(startFactor, startFactor, startFactor);
	}

	public void Update()
	{
		if (transform.localScale.x < endFactor)
		{
			transform.localScale += new Vector3(sizeSpeed, sizeSpeed, sizeSpeed);
		}
	}
}
