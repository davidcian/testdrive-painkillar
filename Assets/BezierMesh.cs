﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class BezierMesh : MonoBehaviour
{
	private System.Random randomGen = new System.Random();

	public BezierSpline parent;

	public float radius;

	public int circularGranularity;

	public int linearGranularity;

	private List<Vector3> vertices = new List<Vector3>();

	private List<int> triangles = new List<int>();

	private List<Vector3> centerPoints = new List<Vector3>();

	private Mesh mesh;

	private void Awake()
	{
		Generate();
	}

	private void Generate()
	{
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Bezier Grid";

		for (int i = 0; i < 1; i++)
		{
			GenerateCurve(i);
		}
	}

	public void GenerateCurve(int index)
	{
		if (index < 0 || index >= parent.CurveCount) throw new ArgumentOutOfRangeException();
		float linearUnit = 1f / (float)linearGranularity;
		for (int i = 0; i <= linearGranularity; i++)
		{
			float t = index + i * linearUnit;
			centerPoints.Add(parent.GetPoint(t));
			Vector3 normal = parent.GetDirection(t);
			float z = -(normal.x + normal.y) / normal.z;
			Vector3 generator = new Vector3(1, 1, z);
			float angleUnit = 360f / (float)circularGranularity;
			for (int j = 0; j < circularGranularity; j++)
			{
				vertices.Add(transform.InverseTransformPoint(centerPoints[index * linearGranularity + i] +
					(Quaternion.AngleAxis(j * angleUnit, normal) * generator.normalized * radius)));
			}
		}

		mesh.vertices = vertices.ToArray();

		for (int i = index * linearGranularity; i < (index + 1) * linearGranularity - 1; i++)
		{
			i = 0;
			for (int j = 0; j < circularGranularity - 1; j++)
			{
				AddRectangle(i * circularGranularity + j, i * circularGranularity + j + 1,
					(i + 1) * circularGranularity + j + 1, (i + 1) * circularGranularity + j);
			}
		}

		mesh.triangles = triangles.ToArray();

		mesh.RecalculateNormals();
	}

	private void AddRectangle(int a, int b, int c, int d)
	{
		AddTriangle(a, b, d);
		AddTriangle(b, c, d);
	}

	private void AddTriangle(int a, int b, int c)
	{
		triangles.Add(a);
		triangles.Add(b);
		triangles.Add(c);
	}

	private void OnDrawGizmos()
	{
		if (vertices == null || centerPoints == null)
		{
			return;
		}
		Gizmos.color = Color.red;
		for (int i = 0; i < vertices.Count; i++)
		{
			Gizmos.DrawSphere(transform.position + vertices[i], 0.2f);
		}
		Gizmos.color = Color.green;
		for (int i = 0; i < centerPoints.Count; i++)
		{
			float t = i * (1f / (float)linearGranularity);

			Gizmos.DrawSphere(centerPoints[i], 0.2f);
			Gizmos.DrawLine(centerPoints[i], centerPoints[i] + parent.GetDirection(t));
		}
	}
}
