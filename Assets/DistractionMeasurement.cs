﻿using UnityEngine;

public class DistractionMeasurement : MonoBehaviour
{
	public SplineWalker walker;

	public Camera view;

	public BezierSpline path;

	public float GetDistractionLevel()
	{
		return 0.1f * Vector3.Angle(path.GetDirection(walker.getProgress()), view.transform.forward);
	}
}
