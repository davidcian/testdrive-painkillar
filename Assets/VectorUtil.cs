﻿using System;

using UnityEngine;

class VectorUtil
{
	private static System.Random randomGen = new System.Random();

	private static double NextDouble(double min, double max)
	{
		if (max < min) throw new ArgumentException("The max has to be bigger or equal to the min");
		return randomGen.NextDouble() * (max - min) + min;
	}

	public static Vector3 RandomVector3()
	{
		return new Vector3(randomGen.Next(10), randomGen.Next(10), randomGen.Next(10));
	}

	public static Vector3 RandomVector3(Vector3 center, float radius)
	{
		double theta = randomGen.NextDouble() * Math.PI;
		double phi = randomGen.NextDouble() * 2 * Math.PI;
		double r = randomGen.NextDouble() * radius;

		return center + sphericCoordVector3((float)r, (float)theta, (float)phi);
	}

	public static Vector3 RandomVector3(double minR, double maxR, double minTheta, double maxTheta, double minPhi, double maxPhi)
	{
		double r = NextDouble(minR, maxR);
		double theta = NextDouble(minTheta, maxTheta);
		double phi = NextDouble(minPhi, maxPhi);

		return sphericCoordVector3((float) r, (float)theta, (float)phi);
	}

	private static Vector3 sphericCoordVector3(float r, float theta, float phi)
	{
		return new Vector3(r * Mathf.Sin(theta) * Mathf.Cos(phi), r * Mathf.Sin(theta) * Mathf.Sin(phi), r * Mathf.Cos(theta));
	}
}
