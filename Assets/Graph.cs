﻿using UnityEngine;

public class Graph : MonoBehaviour
{
	public Transform pointPrefab;

	Transform[] points;

	[Range(10, 100)]
	public int resolution = 10;

	public DistractionMeasurement provider;

	private void Awake()
	{
		float step = 2f / resolution;
		Vector3 scale = Vector3.one / 5f;
		Vector3 position;
		position.y = 0f;
		position.z = 0f;
		points = new Transform[resolution];
		for (int i = 0; i < resolution; i++)
		{
			Transform point = Instantiate(pointPrefab);
			position.x = (i + 0.5f) * step - 1f;
			point.localPosition = position;
			point.localScale = scale;
			point.SetParent(transform, false);
			points[i] = point;
		}
	}

	void Update()
	{
		for (int i = 0; i < points.Length - 1; i++)
		{
			Transform point = points[i];
			Vector3 position = point.localPosition;
			position.y = points[i + 1].localPosition.y;
			point.localPosition = position;
		}

		Vector3 lastPosition = points[points.Length - 1].localPosition;
		lastPosition.y = provider.GetDistractionLevel();
		points[points.Length - 1].localPosition = lastPosition;
	}
}
