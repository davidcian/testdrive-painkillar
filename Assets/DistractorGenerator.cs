﻿using System;

using UnityEngine;

public class DistractorGenerator : MonoBehaviour
{
	public BezierSpline path;

	public Camera playerView;

	public Transform distractorPrefab;

	public int frequencyProb = 50;

	public float minR, maxR;

	private System.Random randomGen = new System.Random();

	public void Update()
	{
		int rand = randomGen.Next(100);

		float theta = playerView.fieldOfView * playerView.aspect;

		if (rand <= frequencyProb)
		{
			Vector3 position;
			Bounds bounds;

			position = VectorUtil.RandomVector3(
				minR, maxR,
				Math.PI - theta, Math.PI + theta,
				Math.PI - playerView.fieldOfView, Math.PI + playerView.fieldOfView);
			bounds = new Bounds(position, distractorPrefab.transform.localScale);

			

			SummonDistractor(position);
		}
	}

	public void SummonDistractor(Vector3 localPos)
	{
		Instantiate(distractorPrefab, playerView.transform.TransformPoint(localPos), Quaternion.identity);
	}
}
