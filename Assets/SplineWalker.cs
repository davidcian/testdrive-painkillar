﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SplineWalker : MonoBehaviour {
	public BezierSpline spline;

	public float speed;

	private float progress;

	public float dispersionRadius;

	public int generateAhead = 3;

	public float getProgress()
	{
		return progress;
	}

	void Update () {
		progress += Time.deltaTime * speed;
		if (progress > spline.CurveCount - 1)
		{
			for (int i = 0; i < generateAhead; i++)
			{
				Vector3 point1 = VectorUtil.RandomVector3(spline.GetControlPoint(spline.ControlPointCount - 1), dispersionRadius);
				Vector3 point2 = VectorUtil.RandomVector3(point1, dispersionRadius);
				Vector3 point3 = VectorUtil.RandomVector3(point2, dispersionRadius);

				spline.AddCurve(point1, point2, point3);
			}
		}

		Vector3 position = spline.GetPoint(progress) + 2 * Vector3.up;
		transform.localPosition = position;
	}
}
